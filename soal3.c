#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <dirent.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/unistd.h>
#include <sys/wait.h>

int main(void){
  pid_t cid1, cid2, cid3, cid4, cid5, cid6;
  int status;

  cid1 = fork(); // digunakan untuk membuat child proses pertama
  if(cid1 < 0){
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if(cid1 == 0){
    char *argv[] = {"mkdir", "-p", "darat", NULL}; // digunakan untuk membuat folder darat
    execv("/bin/mkdir", argv);
  } else{
    while((wait(&status)) > 0);

    cid2 = fork(); // digunakan untuk membuat child proses kedua
    if(cid2 < 0){
      exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if(cid2 == 0){
      sleep(3); // digunakan untuk membuat proses terhenti selama 3 detik
      char *argv[] = {"mkdir", "-p", "air", NULL};
      execv("/bin/mkdir", argv); // digunakan untuk membuat folder air
    } else{
      while((wait(&status)) > 0);

      cid3 = fork(); // digunakan untuk membuat child proses ketiga
      if(cid3 < 0){
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
      }

      if(cid3 == 0){
        char *argv[] = {"unzip", "-q", "animal.zip", NULL};
        execv("/usr/bin/unzip", argv); // digunakan untuk mrngunzip file animal.zip
      } else{
        while((wait(&status)) > 0);

        cid4 = fork(); // digunakan untuk membuat child proses keempat
        if(cid4 < 0){
          exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
   }
  }
 }
}

